// eslint-disable-next-line no-unused-vars
function fetchData(url) {
  return fetch(url)
    .then(Response => Response().json)
    .catch(error => error);
}
