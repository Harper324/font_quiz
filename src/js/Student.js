class Student {
  constructor(name, age, aboutMe, education) {
    this.name = name;
    this.age = age;
    this.aboutMe = aboutMe;
    this.education = education;
  }
}
