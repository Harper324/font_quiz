import './Student';
import './Education';
import './fetchData';

const URL = 'http://localhost:3000/person';
// eslint-disable-next-line no-undef
fetchData(URL)
  .then(result => {
    let student = createStudent(result);
    renderHeader(student);

    //    TODO:handle result
  })
  .catch(error => {
    console.error(error);
  });

function createStudent(result) {
  return new Student(
    result.name,
    result.age,
    result.aboutMe,
    createEducation(result)
  );
}

function renderHeader(student) {
  document.getElementById(
    'name&age'
  ).innerHTML = `MY NAME IS ${student.name} ${student.age}YO AND THIS IS MY CV`;
}

function createEducation(result) {
  var educations = result.education;
  var educationClassArray = [];
  for (let i = 0; i < educations.length; i++) {
    let year = educations[i].year;
    let title = educations[i].title;
    let description = educations[i].description;
    educationClassArray
      // eslint-disable-next-line no-undef
      .push(new Education(year, title, description));
  }

  return educationClassArray;
}
